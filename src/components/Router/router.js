import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Blog from '../../components/blog/blog';
import Navbar from '../../components/Navbar/navbar';


function Routing() {
    return(
        <Router>
            <Switch>
                <Route exact path='/'>
                    <Navbar />
                </Route>
            </Switch>
        </Router>
    )
    
}

export default Routing;
