import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './style.scss'

export default function Navbar() {

    const [isActive, setActive] = useState(false);

    const toggleClass = () => {
        setActive(!isActive);
    };



    return (
        <nav>
            <ul className={isActive ? "active": null}>
                <li>
                    <Link to="path1">Home</Link>
                </li>
                <li>
                    <Link to="path2">About</Link>
                </li>
                <li>
                    <Link to="path3">Contact</Link>
                </li>
                <li>
                    <Link to="path4">Features</Link>
                </li>
            </ul>
            <div className= "mobile-bar" onClick={toggleClass}>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </nav>
    )
}